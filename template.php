<?php
/**
 * Custom theme overrides for Photogenic.
 */

/**
 * Overrides template_preprocess_node().
 */
function photogenic_preprocess_node(&$variables) {
  $node = $variables['node'];

  // Create new teaser_date variables
  $variables['teaser_date']['day'] = format_date($node->created, 'custom', 'd');
  $variables['teaser_date']['month'] = format_date($node->created, 'custom', 'M');
  $variables['teaser_date']['year'] = format_date($node->created, 'custom', 'Y');
}

