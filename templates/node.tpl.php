<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if ($teaser): ?>
    <div class="teaser-date">
      <div class="day"><?php print $teaser_date['day']; ?></div>
      <div class="month"><?php print $teaser_date['month']; ?></div>
      <div class="year"><?php print $teaser_date['year']; ?></div>
    </div>
    <div class="teaser-content">
  <?php endif; ?>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted && $page): ?>
    <div class="submitted"><?php print $submitted; ?></div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

  <?php if ($teaser): ?>
    </div> <!-- /.teaser-content -->
  <?php endif; ?>

</div> <!-- /.node -->

